<h1>Command line instructions</h1>


<h3>Git global setup</h3>

<p>git config --global user.name "Ricardo Ramos dos Santos"</p>
<p>git config --global user.email "ramos.ti@gmail.com"</p>

<br>
<h3>Create a new repository</h3>

<p>git clone git@gitlab.com:magrathea/ecmascript6.git</p>
<p>cd ecmascript6</p>
<p>touch README.md</p>
<p>git add README.md</p>
<p>git commit -m "add README"</p>
<p>git push -u origin master</p>

<br>
<h3>Existing folder</h3>

<p>cd existing_folder</p>
<p>git init<p>
<p>git remote add origin git@gitlab.com:magrathea/ecmascript6.git</p>
<p>git add .</p>
<p>git commit -m "Initial commit"</p>
<p>git push -u origin master</p>

<br>
<h3>Existing Git repository</h3>

<p>cd existing_repo</p>
<p>git remote add origin git@gitlab.com:magrathea/ecmascript6.git</p>
<p>git push -u origin --all</p>
<p>git push -u origin --tags</p>